﻿require('./gulp/gulp.require.js');

gulp.task('default', function () {
    runSequence(['sass', 'fileinclude', 'watch'])
});

gulp.task('sass', function () {
    gulp.src('sass/main.scss')
        .pipe(sass())
        .pipe(gulp.dest(config.dist.css));
});

gulp.task('watch', function () {
    gulp.watch(
        [config.src.style.sass, config.src.html.pages],
        ['sass', 'fileinclude']
    );
});

gulp.task('fileinclude', function () {
    // Windows Phone
    gulp.src([config.src.html.winApp])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(rename('default.html'))
        .pipe(gulp.dest(config.dist.html.winApp));

    // Windows - TODO
});