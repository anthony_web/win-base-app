﻿(function () {
    "use strict";
    if (!window.PLWinApp) { window.PLWinApp = {}; }
    window.PLWinApp.core = {};
    window.PLWinApp.app = {};

}());

(function (app, core) {

    "use strict";
    /**
	 * Range of utility mehods for use across JavaScript builds
	 */
    core.utils = {

        /**
	     * Call a function by its string name. Also works with namespaces
         * @return { } Returns the function call
         * @param {String} functionName - name of the function
         * @param {Object} context - Context of the function (_self, window, ...)
         * @param {args} Pass arguments to the function
	     */
        callFunctionByString: function (functionName, context /*, args */) {
            var args = Array.prototype.slice.call(arguments, 2);
            var namespaces = functionName.split(".");
            var func = namespaces.pop();
            for (var i = 0; i < namespaces.length; i++) {
                context = context[namespaces[i]];
            }
            return context[func];
        }

    };

}(PLWinApp.app, PLWinApp.core));