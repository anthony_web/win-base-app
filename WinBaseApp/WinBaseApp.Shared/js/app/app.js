﻿(function (winApp, app, core) {

    /**
	 * constructor of the navigation, collect all the elements necessary
	 * and then set listeners on these
	 *
	 * @param {object} element DOM element on which ti init widget
	 * @param {object} config configuration object to init with
	 * @constructor
	 */
    app.main = function () {

        var _self = this;

        // elements
        _self.debug = document.getElementById('debug');
        _self.template = document.getElementById('template');
        _self.content = document.getElementById('content');

        _self.initContent();

    };

    app.main.prototype.initContent = function () {

        var _self = this;

        _self.template.innerHTML = ''; // reset content

        //loadFragment(_self, 'leagueTable');

    };

    var loadFragment = function (_self, name) {
        // Read fragment from the HTML file and load it into the #content div
        WinJS.UI.Fragments.renderCopy('/html/pages/' + name + '.html', _self.template)
            .done(function () {
                // call constructor of new page
                var object = core.utils.callFunctionByString(name, app);
                new object();
            },
            function (error) {
                console.log('error loading fragment: ' + error, label, 'error');
            });
    };

})(WinJS.Application, PLWinApp.app, PLWinApp.core);