﻿(function (app) {
    "use strict";

    app.templates = {};
    app.common = {};
    app.defaultLanguage = "EN";

    app.paths = [
		{
		    label: "local",
		    domain: "localhost",
		    api: "http://api.test.platform.pulselive.com/football"
		},
		{
		    label: "development",
		    domain: "testpl.pulselive.com",
		    cdn: "//cdn.pulselive.com/test/client/pl/dev/"
		},
		{
		    label: "test",
		    domain: "test2pl.pulselive.com",
		    cdn: "//cdn.pulselive.com/test/client/pl/test/"
		},
		{
		    label: "staging",
		    domain: "staging-base.pulselive.com",
		    cdn: "//cdn.pulselive.com/dynamic/client/pl/staging/"
		},
		{
		    label: "production",
		    domain: "base.pulselive.com",
		    cdn: "//cdn.pulselive.com/dynamic/client/pl/prod/"
		}
    ];

    app.auth = {

    };

    app.checkEnvironment = function () {

        var location = window.location.hostname;
        var environment;

        app.paths.map(function (path) {
            if (location === path.domain) {
                environment = path;
            }
        });

        return environment || app.paths[0];

    };

    app.environment = app.checkEnvironment();

}(PLWinApp.app));